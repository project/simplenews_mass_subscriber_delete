Description :
------------

This module basically deletes all the subscribers of Simplenews.


REQUIREMENTS :
--------------

1. Simplenews Module


Installation :
--------------

1. Download the project from https://www.drupal.org/project/simplenews_mass_subscriber_delete and unzip the project.
2. Place the project under '/sites/all/modules/contrib' directory and enable the module.

CONTACT :
---------

Current maintainers:
  * Pankaj Sachdeva (pankajsachdeva) - http://drupal.org/u/pankajsachdeva


This project has been sponsored by:
  * ]INIT[ AG
    Visit https://www.init.de/en for more information.
